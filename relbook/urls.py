# coding: utf-8

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView

from .registration.views import get_friendlist, tracklist

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$',  TemplateView.as_view(template_name = 'index.html'), name = 'index'),
    url(r'^friend_list/$', get_friendlist),
    url(r'^tracklist/$', tracklist),
    url(r'^social/', include('social_auth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    )

# EOF
