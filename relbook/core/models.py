# # coding: utf-8

from django.db import models
from django.contrib.auth.models import AbstractUser


# AbstractUser._meta.get_field('is_active').default = False

class User(AbstractUser):
    phone = models.CharField(u'Телефон', max_length = 64, blank = True)

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'


# # EOF
