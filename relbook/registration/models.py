# coding: utf-8

from django.conf import settings
from django.db import models
from django.utils import simplejson

from social_auth.fields import JSONField


relation_choices = {
    0: u'не указано',
    1: u'не женат/не замужем',
    2: u'есть друг/есть подруга',
    3: u'помолвлен/помолвлена',
    4: u'женат/замужем',
    5: u'всё сложно',
    6: u'в активном поиске',
    7: u'влюблён/влюблена',
    }


sex_choices = {
    0: u'пол не указан',
    1: u'женский',
    2: u'мужской',
    }


class FriendList(models.Model):
    vk_id = models.IntegerField(u'id', db_index = True)
    relation = models.IntegerField(u'Семейное положение', default = 0, choices = relation_choices.items())
    sex = models.IntegerField(u'Пол', default = 0, choices = sex_choices.items())
    data = JSONField(u'Данные', blank = True)
    track = models.BooleanField(u'Отслеживать', default = False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name = u'Пользователь', related_name = 'friend_set')
    is_changed = models.BooleanField(u'Изменен', default = False)

    class Meta:
        unique_together = ['vk_id', 'user']
    #     verbose_name
    #     verbose_name_plural

    def __unicode__(self):
        return u'%s %s' %(self.data.get('first_name'), self.data.get('last_name'))




# EOF
